
/*var formidable = require('formidable'),
util = require('util'),
fs   = require('fs');*/
const dataStoreObj = require('./uploaderdatastore.js');
const excelutils = require('./excelutils.js');

const path = require('path');
const os = require('os');
const fs = require('fs');
const Busboy = require('busboy');
const PubSub = require('@google-cloud/pubsub');
const pubsub = new PubSub();
const topic = pubsub.topic('vision_topic');
const publisher = topic.publisher();

exports.uploaderendpoints = function endPoint(req, res) {

	
	var crypto = require('./crypto');
	var jsonEncrypted = req.query.token;
	try {
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "*");
		res.header("Access-Control-Allow-Methods"," POST, PUT, DELETE, GET, OPTIONS");
		
		var json = crypto.decrypt(jsonEncrypted);
		var jsonObject = JSON.parse(json);
		var email = jsonObject.email;
			
		if (req.method == 'POST') {
				
				var busboy = new Busboy({ headers: req.headers });
			    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {				      
			      tmpfilename = "/tmp/"+filename+String(new Date().getTime());				     
			      var wstream = fs.createWriteStream(tmpfilename);
  				  var filebuffer= [];
			      file.on('data', function(data) {
			      	filebuffer.push(data);
			        
			      });
			      file.on('end', function() { 			      	
				        databuffer = Buffer.concat(filebuffer);
						wstream.write(databuffer);
						wstream.end(function () { 
							fileupload(databuffer,tmpfilename,filename,res,email);							
							
						});	
			      });
			    });
			    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
			      
			    });
			    busboy.on('finish', function() {
			      
			    });
			   
			    busboy.end(req.rawBody);
			    req.pipe(busboy);
		} else if (req.method == 'OPTIONS') {
			//console.log("inside OPTIONS");
			res.status(200).send(JSON.stringify({ "status": "ok" ,"message":"CORS_REQUEST"}));
		} else if (req.method == 'PUT') {
			if(req.body.operationKey==='updateStatus'){

				update_record_status(req,res,email)
			}else{
				update_temp_record(req,res,email);	
			}
					
		} else if (req.method == 'GET') {
			templateId  = req.query.id;
			if(templateId && templateId.length>0){
				dataStoreObj.getRecord(templateId).then(results=>{
					const entities = results[0];
					res.status(200).send(entities);

				});

			}else{
				dataStoreObj.getAllRecords('ALL').then(results=>{
					const entities = results[0];				
					entities.length > 0 ? res.status(200).send(filter_status(entities)) :
					res.status(200).send(JSON.stringify({ "status": "ok" , "message":"No Data Found"}));		

				}).catch(err =>{
					console.log(err);
					res.status(400).send(JSON.stringify({ "status": "error" , "message":"Error while loading data"}));
				});
			}
		}else {
			res.status(405).end();
		}

	}catch (err) {
		console.log(err)
		res.redirect('/visionlogin?signout=1');
	}
		
};

function filter_status(entities){	
	var results = [];
	if (entities && entities.length){
		entities.forEach(entity => {
			const taskKey = entity[dataStoreObj.datastore.KEY];
			entity.id=taskKey.id;
			entity.inputFile='';
			entity.status != 'TEMP' ?  results.push(entity) : null;			
		 });
		
	}
	return results;	

}
function getSimpleQuery(key,value){
	var queryObj =  dataStoreObj.datastore.createQuery('Task');
	for (var i = 0; i < arguments.length; i=i+2)
		queryObj.filter(arguments[i], '=', arguments[i+1]);
	return queryObj.order(key);
}


function fileupload(file,tmpfilename,filename,res,email){
	response = dataStoreObj.uploadfile(file,email);
	key =  response[1];
	response = response[0];
	response.then(()=>{
		taskkey =key.path.pop();
		excelutils.getSheetNames(tmpfilename,filename,taskkey,res);
	}).catch(err => {
    	console.error('ERROR:', err);
    	res.status(200).send(JSON.stringify({ "status": "error" , "message":err}));
  });
}

function update_temp_record(req,res,email){
	id = req.body.id;
	dataStoreObj.getRecord(id).then(results =>  {
		const task = results[0];
		if (task) {
			
			task.rfp_name=req.body.rfp_name;
			task.product_version=req.body.product_version;
			task.uploadedUser = email;
			task.uploadedTime= Date();
			task.sheets = JSON.stringify(req.body.sheets);
			task.status = 'UPLOADED';
			dataStoreObj.update_temp_record(id,task).then(() => {
				publishmessage(req.body.filepath,req.body.sheets,id);				
				res.status(200).send(JSON.stringify({ "status": "ok" , "message":"Saved successfully"}));						
			});					
		}else{
			res.status(400).send(JSON.stringify({ "status": "warning" ,"message":"Uploaded File is not available"}));
		}
								
	});	
	
}

function update_record_status(req,res,email){

	id = req.body.id;
	status = req.body.status;
	dataStoreObj.getRecord(id).then(results =>  {
		const task = results[0];
		if (task) {		
			task.status = status
			dataStoreObj.update_temp_record(id,task).then(() => {
				res.status(200).send(JSON.stringify({ "status": "ok" , "message":"Status updated successfully"}));						
			});					
		}else{
			res.status(400).send(JSON.stringify({ "status": "warning" ,"message":"Uploaded File is not available"}));
		}
								
	});	

}

function publishmessage(filepath,sheets,id){
	console.log("publish",filepath,id);
	data_value = JSON.stringify({ "filepath": filepath , "sheetconfig":sheets,"id":id});	
	const data = Buffer.from(data_value);
	const callback = function(err, messageId) {
		console.log("publish id ",messageId)
	  if (err) {
	   console.log("error while publishing");
	  }
	};

	publisher.publish(data, callback);
}
