var Excel = require('exceljs');
var path = require('path')
var workbook = new Excel.Workbook();
const fs = require('fs');

exports.getSheetNames = function (file,basename,id,res) {  
  workbook.xlsx.readFile(file).then(function() {
    var filename = path.basename(file);
    var fileDetails = {
      id: id,
      filename:file,
      name:basename,
    };
    var sheets = [];
    workbook.eachSheet(function(worksheet) {
      sheets.push(worksheet.name);
    });
    fileDetails.sheets = sheets;    
    res.status(200).send(JSON.stringify(fileDetails));
  });
};

