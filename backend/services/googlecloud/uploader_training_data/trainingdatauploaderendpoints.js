
/*var formidable = require('formidable'),
util = require('util'),
fs   = require('fs');*/

const Busboy = require('busboy');
const PubSub = require('@google-cloud/pubsub');
const pubsub = new PubSub();
const topic = pubsub.topic('vision_topic');
const publisher = topic.publisher();

exports.trainingdatauploaderendpoints = function endPoint(req, res) {

	
	var crypto = require('./crypto');
	var jsonEncrypted = req.query.token;
	try {
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "*");
		res.header("Access-Control-Allow-Methods"," POST, PUT, DELETE, GET, OPTIONS");
		
		var json = crypto.decrypt(jsonEncrypted);
		var jsonObject = JSON.parse(json);
		var email = jsonObject.email;
			
		if (req.method == 'POST') {
				
				var busboy = new Busboy({ headers: req.headers });
				var fields ={};
				var databuffer ;
			    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {				      
			 	  var filebuffer= [];
  				  file.on('data', function(data) {			      
			      	filebuffer.push(data);
			        
			      });
			      file.on('end', function() { 			      	
				       databuffer = Buffer.concat(filebuffer);
					
			      });
			    });
			    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {			  	 
			  	   fields = val;	  	 
			      
			    });
			    busboy.on('finish', function() {
			      publishmessage(databuffer,fields,email,res);	
			    });
			   
			    busboy.end(req.rawBody);
			    req.pipe(busboy);
		} else if (req.method == 'OPTIONS') {
			//console.log("inside OPTIONS");
			res.status(200).send(JSON.stringify({ "status": "ok" ,"message":"CORS_REQUEST"}));
		}else {
			res.status(405).end();
		}

	}catch (err) {
		console.log(err)
		res.redirect('/visionlogin?signout=1');
	}
		
};


function publishmessage(databuffer,data,email,res){
	
	data = JSON.parse(data);
	data.fileBuffer = databuffer;
	data.key = 'manual_entry';
	data.email=email;
	
	const message = Buffer.from(JSON.stringify(data));
	const callback = function(err, messageId) {
		console.log("publish id ",messageId)
	  if (err) {
	   console.log("error while publishing");
	  }
	};

	publisher.publish(message, callback);
	res.status(200).send(JSON.stringify({ "status": "Success" , "message": "Document uploaded"}));
}
