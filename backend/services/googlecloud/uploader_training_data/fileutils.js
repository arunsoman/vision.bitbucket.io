const XlsxPopulate = require('xlsx-populate');
const fs = require('fs');

var express = require('express');
var app = express();
app.get('/', function (req, res) {
    process_excel(res);
    console.log('process excel');

});

var server = app.listen(8082, function () {});

function process_excel(res) {
    var ques_arr = [];

    // Load an existing workbook
    XlsxPopulate.fromFileAsync("./input.xlsx")
        .then(workbook => {
            const value = workbook.sheet(0);
            for (var index = 2; index < value._rows.length; index++) {
                var ques_obj = {
                    question: '',
                    answer: '',
                    docLink: '',
                    featureStatus: '',
                }

                if(value._rows[index]){
                    value._rows[index]._cells.forEach(function(inner){
                        if (inner._columnNumber == 1) {
                                ques_obj.question = inner._value;
                            } else if (inner._columnNumber == 3) {
                                ques_obj.answer = inner._value
                            } else if (inner._columnNumber == 2) {
                                ques_obj.featureStatus = inner._value
                            } else if (inner._columnNumber == 4) {
                                ques_obj.docLink = inner._value
                            }
                    })
                    // console.log(ques_obj);
                    ques_arr.push(ques_obj);
                }



            }
        });
}
