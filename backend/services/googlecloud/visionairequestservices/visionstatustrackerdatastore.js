const Datastore = require('@google-cloud/datastore');
const projectId = 'complete-land-188108';

const datastore = new Datastore({
		projectId: projectId,
});

exports.datastore=datastore;

exports.uploadfile = function(file){
	const taskKey = datastore.key('RFP_REQUESTS');
	const entity = {
		key: taskKey,
		excludeFromIndexes: ['inputFile'],
		data:{
			inputFile:file,
			status:'TEMP',		
		}
	};
	return [datastore.insert(entity),taskKey];
};

exports.update_temp_record = function(id,task){
	const taskKey = datastore.key(['RFP_REQUESTS', parseInt(id)]);
	const entity = {
		key: taskKey,
		excludeFromIndexes: ['inputFile','sheets'],
		data: task,
	};
	
	return datastore.update(entity);
};

exports.saveRecord = function (rfpName,productVersion,uploadedUser,uploadedTime,templateJson,inputFile,replayFile,status){
	console.log("inside save record")
	task = getEntity(rfpName,productVersion,uploadedUser,uploadedTime,templateJson,inputFile,replayFile,status);
	const taskKey = datastore.key('RFP_REQUESTS');
	const entity ={
		key: taskKey,
		excludeFromIndexes: ['inputFile','sheets','replayFile'],
		data: task,
	};
	return datastore.insert(entity);
};

exports.getRecord = function (taskId) {
	const taskKey = datastore.key(['RFP_REQUESTS', parseInt(taskId)]);
	return datastore.get(taskKey); 
};

exports.getQueryResult = function (query) {
	return datastore.runQuery(query);
};


function getEntity(rfpName,productVersion,uploadedUser,uploadedTime,templateJson,inputFile,replayFile,status){	
	RFP_REQUESTS = {	
		rfpName :rfpName,
		productVersion:productVersion,
		uploadedUser:uploadedUser,
		uploadedTime:uploadedTime,
		sheets:templateJson,
		inputFile:inputFile,
		replayFile:replayFile,
		status:status,
	};
	return RFP_REQUESTS;
}


exports.deleteRecord = function  (taskId) {
	const taskKey = datastore.key(['RFP_REQUESTS', parseInt(taskId)]);
	return datastore.delete(taskKey);
};


exports.getAllRecords = function (type) {	
	if (type=='OPEN') {
		const query = datastore.createQuery('RFP_REQUESTS').filter('status','=','NEW');
		return datastore.runQuery(query);  
	}else if (type=='CLOSE')  {
		const query = datastore.createQuery('RFP_REQUESTS').filter('status','=','CLOSE');
		return datastore.runQuery(query);
	}else{
		const query = datastore.createQuery('RFP_REQUESTS');
		return datastore.runQuery(query);
	}		
};

exports.updateMultipleRecord = function (questions){
	// task = getEntity(rfpName,productVersion,uploadedUser,uploadedTime,templateJson,inputFile,replayFile,status);
	
	questions.forEach(value => {	
		const taskKey = datastore.key(['AI_REQUESTS', parseInt(value.id)]);
		delete value.id;
		const entity ={
			key: taskKey,
			excludeFromIndexes: ['question','answers'],
			data: value,
		};
		datastore.update(entity).then({

			}).catch(err => {
					console.log('ERROR while update questions:', err);
			});

	});

	
	
};

exports.markasDeleted = function (updateEntity){
	taskKey = updateEntity[datastore.KEY];
	dataEntity = getEntity(updateEntity.projectName,updateEntity.projectState,updateEntity.docVersion,updateEntity.modificationTime,updateEntity.createdUser,updateEntity.modifiedUser,updateEntity.inputData,updateEntity.parentId);
	dataEntity.status ='DELETED';
	const entity ={
		key: taskKey,
		excludeFromIndexes: ['inputData'],
		data: dataEntity,
	};
	return datastore.update(entity);
};
