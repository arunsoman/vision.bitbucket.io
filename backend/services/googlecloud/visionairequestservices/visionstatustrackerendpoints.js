
/*var formidable = require('formidable'),
util = require('util'),
fs   = require('fs');*/
const dataStoreObj = require('./visionstatustrackerdatastore.js');
const excelutils = require('./excelprocess.js');

exports.visionstatustrackerendpoints = function endPoint(req, res) {

	
	var crypto = require('./crypto');
	var jsonEncrypted = req.query.token;
	try {
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "*");
		res.header("Access-Control-Allow-Methods"," POST, PUT, DELETE, GET, OPTIONS");
		
		var json = crypto.decrypt(jsonEncrypted);
		var jsonObject = JSON.parse(json);
		var email = jsonObject.email;
			
		if (req.method == 'POST') {
			var templateRefId = req.body.templateRefId;
			var sheetName =req.body.sheetName;
			console.log(templateRefId,sheetName);
			query = dataStoreObj.datastore.createQuery('AI_REQUESTS').filter('templateRefId', '=', templateRefId).filter('sheetName','=',sheetName).order('rowNumber');
			dataStoreObj.getQueryResult(query).then(results => {
				const entities = results[0];
				if (entities.length > 0){
					entities.forEach(entity => {
						const taskKey = entity[dataStoreObj.datastore.KEY];
						entity.id=taskKey.id;
		 			});
					res.status(200).send(entities); 					
				}else{
					res.status(400).send(JSON.stringify({ "status": "warning" ,"message":"No Record Found"}));								
				}
			});
				
		} else if (req.method == 'OPTIONS') {
			//console.log("inside OPTIONS");
			res.status(200).send(JSON.stringify({ "status": "ok" ,"message":"CORS_REQUEST"}));
		} else if (req.method == 'PUT') {
			if (req.body.postKey =='saveQuestions'){
				questions = req.body.questions;
				console.log("type",typeof(questions));
				dataStoreObj.updateMultipleRecord(questions);
				res.status(200).send(JSON.stringify({ "status": "success" ,"message":"Updation Request Processed"}));	
			}else{
				res.status(400).send(JSON.stringify({ "status": "error" ,"message":"Please specify the action properly"}));
			}
			
						
		} else if (req.method == 'GET') {
			console.log("inside get");
			id = req.query.id;
			action  = req.query.action;
			if (action =='download'){
				excelutils.process_excel(id,res);
			}else{
				res.status(400).send(JSON.stringify({ "status": "error" ,"message":"Please specify the action properly"}));
			}

			
		}else {
			res.status(405).end();
		}

	}catch (err) {
		console.log(err)
		res.redirect('/visionlogin?signout=1');
	}
		
};

