const fs = require('fs');
var path = require('path')
const async = require('async');
const XlsxPopulate = require('xlsx-populate');
const dataStoreObj = require('./visionstatustrackerdatastore.js');

function call_back(message) {
	res.status(400).send(JSON.stringify({
		"status": "error",
		"message": message
	}));
}



exports.process_excel =  function(template_ref_id,res) {
	var Workbook = require('xlsx-populate');
	dataStoreObj.getRecord(template_ref_id).then(results => {
		const entity = results[0];
		console.log("inside process",entity);
		if (entity){
			create_excel(entity, template_ref_id,Workbook, res);
			
		}else {
			call_back("Unable to load data from data store ")
		}

	});
	
};

function create_excel(task, id, workbook,res) {
	console.log('create excel')
	file_buffer = task.inputFile;
	data = Buffer.from(file_buffer, 'base64');
	XlsxPopulate.fromDataAsync(data)
		.then(workbook => {
			var sheets = JSON.parse(task.sheets);
			console.log('each series start');
			async.eachSeries(sheets,
				function (sheet, eachcallback) {
					if (sheet.skippable == "false") {
						console.log("sheet name", sheet.sheet_name);
						process_sheet_details(id, sheet, workbook, eachcallback);
					} else {
						console.log();
						eachcallback(null);
					}
				},
				function (err) {
					if (err) {
						console.log('A file failed to process');
					} else {
						console.log('All files have been inner processed successfully');
						workbook.outputAsync().then(data => {
							console.log('res called',task.rfp_name);
							// Set the output file name.
							res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
							res.setHeader('Content-disposition', 'attachment; filename=' + task.rfp_name + ".xlsx");
							// Send the workbook.
							res.status(200).send(data);
							res.end()
						})
					}

				});
		})
}


function process_sheet_details(id, sheet, workbook, eachcallback) {

	query = dataStoreObj.datastore.createQuery('AI_REQUESTS').filter('templateRefId', '=', id).filter('sheetName','=',sheet.sheet_name).order('rowNumber');
	dataStoreObj.getQueryResult(query).then(results => {
		const entities = results[0];
		if (entities.length > 0){
			entities.forEach(entity => {
				const taskKey = entity[dataStoreObj.datastore.KEY];
				entity.id=taskKey.id;
 			});
 			return processQuestions(entities,sheet,workbook,eachcallback);
 			
								
		}else{
			res.status(400).send(JSON.stringify({ "status": "warning" ,"message":"No Questions Found"}));								
		}
	});

}

function getColumn(columns, row) {
	var obj = {};
	Object.keys(columns).forEach(column => {
		row._cells.forEach(cell => {
			if (String(cell._value) == columns[column]) {
				obj[column] = cell._columnNumber
			}
		});
	})
	return obj;
}

// outer comparison
function sortfunction(a, b) {
    if (a.status == 'accept' && b.status == 'accept') {
        return checkfeature(a, b);
    } else if (a.status == 'solution' && b.status == 'solution') {
        return checkfeature(a, b);
    } else {
        var returnval = a.status === 'accept' ? false : b.status === 'accept' ? true : null;
        var returnval = returnval == null ? a.status === 'solution' ? false : b.status === 'solution' ? true : null : returnval;
        return returnval != null ? returnval : checkfeature(a, b);
    }
}

// inner comparison for feature status
function checkfeature(a, b) {
    var returnval = a.featureStatus == 'Fully Compliance' ? false : b.featureStatus == 'Fully Compliance' ? true : null;
    returnval = returnval == null ? a.featureStatus == 'Partially Compliance' ? false : b.featureStatus == 'Partially Compliance' ? true : null : returnval
    returnval = returnval == null ? a.featureStatus == 'Non Compliance' ? false : b.featureStatus == 'Non Compliance' ? true : true : returnval;
    return returnval;
}

function processQuestions(questions, sheet, workbook, eachcallback) {
	worksheet = workbook.sheet(sheet.sheet_name);
	const row = worksheet.row(sheet.header_index);

	var columnindex = getColumn(sheet.columns, row);
	
	for (let question of questions) {
		var sheetrow = worksheet.row(question.rowNumber)
		if (question.answers) {
			var answer = JSON.parse(question.answers)
			answer.sort(this.sortfunction)
			if (columnindex.feature_status != undefined) {
				sheetrow.cell(columnindex.feature_status).value(answer[0].featureStatus);
			}
			if (columnindex.remark != undefined) {
				sheetrow.cell(columnindex.remark).value(answer[0].answer);
			}
			if (columnindex.doc_ref != undefined) {
				sheetrow.cell(columnindex.doc_ref).value(answer[0].doc_ref);
			}
		}
	}
	return eachcallback(null);
}
