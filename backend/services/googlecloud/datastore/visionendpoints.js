const dataStoreObj = require('./visiondatastore.js');
const visionoauth = require('./visionauthorize.js');
const crypto = require('./crypto');
const solutionworkflow = require('./solutionWorkFlow.js');

exports.visionendpoints = function endPoint(req, res) {
	
	try {
		
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "*");
		res.header("Access-Control-Allow-Methods"," POST, PUT, DELETE, GET, OPTIONS");				

		if (req.method == 'POST' && req.query.action==='login' ){
			visionoauth.visionauthorize(req,res);
		}else if (req.method == 'OPTIONS') {
				res.status(200).send(JSON.stringify({ "status": "ok" ,"message":"CROSS_REQUEST"}));
		}else{
			
			var jsonEncrypted = req.query.token;
			var json = crypto.decrypt(jsonEncrypted);
			var jsonObject = JSON.parse(json);
			var email = jsonObject.email;		
			if (req.method == 'GET') {
				invokGetMethod(req,res);
			}else if (req.method == 'POST') {
				requestStatus = req.body.requestStatus=='MANUAL_ENTRY'?req.body.requestStatus:'OPEN';	
				SOLUTION_REQUEST = {		
					question: req.body.question,
					productVersion: req.body.productVersion,
					requestUser:email,
					requestTime:new Date(),
					requestedSource:req.body.requestedSource,
					questionId : req.body.questionId,
					templateRefId :req.body.templateRefId,
					answer:req.body.answer,
					docLink:req.body.docLink,				
					featureStatus : req.body.featureStatus,	
					answeredTime : new Date(),
					answeredUser: email,			
					requestStatus:requestStatus

				};		

				if (mandatoryCheck(res,SOLUTION_REQUEST.question,'Question') && mandatoryCheck(res,SOLUTION_REQUEST.productVersion,'productVersion')){
					dataStoreObj.saveRecord(SOLUTION_REQUEST).then(() => {
						solutionworkflow.updateQuestionStatus(res,SOLUTION_REQUEST);
					});
				}			

			}else if (req.method == 'DELETE') {
				var id = req.query.id;
				if (mandatoryCheck(res,id,'Id')){
					
				}				
			}else if(req.method == 'PUT') {	
				requestStatus = req.body.requestStatus=='MANUAL_ENTRY'?req.body.requestStatus:'CLOSE';
				SOLUTION_REQUEST = {		
					question: req.body.question,
					productVersion: req.body.productVersion,
					requestUser:req.body.requestUser,
					requestTime:new Date(),
					requestedSource:req.body.requestedSource,
					questionId : req.body.questionId,
					templateRefId :req.body.templateRefId,				
					answer:req.body.answer,
					docLink:req.body.docLink,				
					featureStatus : req.body.featureStatus,	
					answeredTime : new Date(),
					answeredUser: email,			
					requestStatus:requestStatus
				};			
				var id = req.body.id;
				if (mandatoryCheck(res,id,'Id')){
					records = dataStoreObj.updateRecord(SOLUTION_REQUEST,id).then(() => {

						//add logic for send mail
						solutionworkflow.updateQuestionAnswer(res,SOLUTION_REQUEST);
					});
				}
			    
			}
		}

	} catch (err) {
		console.log(err)
		res.redirect('/visionlogin?signout=1');
	}
};


function invokGetMethod(req,res){
	var historyOption = req.query.status;	
	records = dataStoreObj.getAllRecords(historyOption).then(results => {
		tasks = results[0];
		tasks.forEach(task => {
			const taskKey = task[dataStoreObj.datastore.KEY];
			task.id=taskKey.id;
		  });
		
		res.status(200).send(tasks);

	});	

}

function getSimpleQuery(key,value){

	var queryObj =  dataStoreObj.datastore.createQuery('Task');
	for (var i = 0; i < arguments.length; i=i+2)
		queryObj.filter(arguments[i], '=', arguments[i+1]);

	return queryObj.order(key);
}



function mandatoryCheck(res,value,fieldName){	
	if (!value || String(value).length == 0){
		res.status(400).send(JSON.stringify({ "status": "error" ,"message":"Mandatory Field Missing - "+fieldName}));
		return false;
	}
	return true;		
}

function filterLowerVersions(entities){

	var projectNames = [];
	var results = [];
	if (entities && entities.length){
		for (var index = 0 ;index < entities.length ; index ++){
			var selectedEntity = entities[index];
			for (var subIndex = index +1 ; subIndex < entities.length ;subIndex++){				
				if ((selectedEntity.projectName == entities[subIndex].projectName ) && (parseFloat(selectedEntity.docVersion) < parseFloat(entities[subIndex].docVersion))){
					selectedEntity = entities[subIndex];					
				}
			}
			if(projectNames.indexOf(selectedEntity.projectName) < 0 ){				
				results.push(selectedEntity);
				projectNames.push(selectedEntity.projectName);
			}

		}
		
	}
	return results;	
}
