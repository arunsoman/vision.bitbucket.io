const Datastore = require('@google-cloud/datastore');
const projectId = 'complete-land-188108';

const datastore = new Datastore({
		projectId: projectId,
});

exports.datastore=datastore;

exports.saveRecord = function (task){
	
	const taskKey = datastore.key('SOLUTION_REQUEST');
	const entity ={
		key: taskKey,
		excludeFromIndexes: ['question','answer'],
		data: task,
	};
	return datastore.insert(entity);
};

exports.getRecord = function (taskId) {
	const taskKey = datastore.key(['SOLUTION_REQUEST', parseInt(taskId)]);
	return datastore.get(taskKey); 
};

exports.getQueryResult = function (query) {
	return datastore.runQuery(query);
};


function getEntity(question,productVersion,requestUser,requestTime,requestStatus){	
	SOLUTION_REQUEST = {		
		question: question,
		productVersion: productVersion,
		requestUser:requestUser,
		requestTime:requestTime,
		requestStatus:requestStatus
				

	};
	return SOLUTION_REQUEST;
}



exports.deleteRecord = function  (taskId) {
		const taskKey = datastore.key(['SOLUTION_REQUEST', parseInt(taskId)]);
		return datastore.delete(taskKey);
};


exports.getAllRecords =function (type) {
	
	if (type=='OPEN') {
		const query = datastore.createQuery('SOLUTION_REQUEST').filter('requestStatus','=','OPEN');
		return datastore.runQuery(query);  
	}else if (type=='CLOSE')  {
		const query = datastore.createQuery('SOLUTION_REQUEST').filter('requestStatus','=','CLOSE');
		return datastore.runQuery(query);
	}else if (type=='MANUAL_ENTRY')  {
		const query = datastore.createQuery('SOLUTION_REQUEST').filter('requestStatus','=','MANUAL_ENTRY');
		return datastore.runQuery(query);
	}else{
		const query = datastore.createQuery('SOLUTION_REQUEST');
		return datastore.runQuery(query);
	}		
};

exports.getQuestion = function (taskId) {
	const taskKey = datastore.key(['AI_REQUESTS', parseInt(taskId)]);
	return datastore.get(taskKey); 
};

exports.updateQuestion= function (question){
	const taskKey = datastore.key(['AI_REQUESTS', parseInt(question.id)]);
	delete question.id;
	const entity ={
		key: taskKey,
		excludeFromIndexes: ['question','answers'],
		data: question,
	};
	return datastore.update(entity);
};
	


exports.updateRecord = function (task,id){	
	const taskKey = datastore.key(['SOLUTION_REQUEST', parseInt(id)]);
	const entity ={
		key: taskKey,
		excludeFromIndexes: ['question','answer'],
		data: task,
	};
	return datastore.update(entity);
};

exports.markasDeleted = function (updateEntity){
	taskKey = updateEntity[datastore.KEY];
	dataEntity = getEntity(updateEntity.projectName,updateEntity.projectState,updateEntity.docVersion,updateEntity.modificationTime,updateEntity.createdUser,updateEntity.modifiedUser,updateEntity.inputData,updateEntity.parentId);
	dataEntity.status ='DELETED';
	const entity ={
		key: taskKey,
		excludeFromIndexes: ['inputData'],
		data: dataEntity,
	};
	return datastore.update(entity);
};
