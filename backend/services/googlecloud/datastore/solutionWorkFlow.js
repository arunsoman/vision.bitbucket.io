
const dataStoreObject = require('./visiondatastore.js');

exports.updateQuestionAnswer = function (res,entity){	
	entity.requestedSource==='RFP' ? update_ai_request_entity(res,entity):res.status(200).send(JSON.stringify({ "status": "ok" ,"message":"Solution updated"}));
};

exports.updateQuestionStatus = function (res,entity){
	entity.requestedSource==='RFP' ? update_ai_request_entity_status(res,entity):res.status(200).send(JSON.stringify({ "status": "ok" ,"message":"Solution updated"}));
};


function update_ai_request_entity_status(res,req_entity){
	dataStoreObject.getQuestion(req_entity.questionId).then(results => {
		const entity = results[0];
		
		if (entity){
			
			const taskKey = entity[dataStoreObject.datastore.KEY];
			entity.id=taskKey.id;		
			entity.finalStatus='solution_waiting';
			dataStoreObject.updateQuestion(entity).then(() => {
				res.status(200).send(JSON.stringify({ "status": "ok" ,"message":"Solution request successfully send"}));
			}).catch(err => {
				console.log('Error while update questions:', err);
				res.status(400).send(JSON.stringify({ "status": "warning" ,"message":"Error while sending solution request"}));	
			});
							
		}else{
			console.log("No question found against the id ",req_entity.questionId);
			res.status(400).send(JSON.stringify({ "status": "warning" ,"message":"Error while sending solution request"}));								
		}
	});
}

function update_ai_request_entity(res,req_entity){

	dataStoreObject.getQuestion(req_entity.questionId).then(results => {
		const entity = results[0];
		if (entity){

			const taskKey = entity[dataStoreObject.datastore.KEY];
			entity.id=taskKey.id; 

			answers =  entity.answers ?  JSON.parse(entity.answers): [];

			solution_ans ={
				author:'solution',
				type:'text',
				answer:req_entity.answer,
				doc_ref:req_entity.docLink,
				featureStatus:req_entity.featureStatus,
				status:'solution',
			};

			answers.push(solution_ans);
			entity.answers = JSON.stringify(answers);
			entity.finalStatus='solution';
			entity.soultionProviedBy='solution';
			
			dataStoreObject.updateQuestion(entity).then(() => {
				res.status(200).send(JSON.stringify({ "status": "ok" ,"message":"Solution updated"}));
			}).catch(err => {
				console.log('Error while update questions:', err);
				res.status(400).send(JSON.stringify({ "status": "warning" ,"message":"Error while processing"}));	
			});
							
		}else{
			res.status(400).send(JSON.stringify({ "status": "warning" ,"message":"No Record Found"}));								
		}
	});

}

