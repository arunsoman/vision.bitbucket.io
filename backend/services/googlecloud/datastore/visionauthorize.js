
const crypto = require('./crypto');

exports.visionauthorize = function authorize(req, res) {
    var token = req.body.token;
    var GoogleAuth = require('google-auth-library');
    var auth = new GoogleAuth;
    var client = new auth.OAuth2("310884155109-4bvcjo9r1njgmqdascm60r2jeuhtgf1o.apps.googleusercontent.com", '', '');
    client.verifyIdToken(token,
        "310884155109-4bvcjo9r1njgmqdascm60r2jeuhtgf1o.apps.googleusercontent.com",
        function (e, login) {
            if (login !== undefined) {
                var payload = login.getPayload();
                var domain = payload['hd'];
                if (domain !== undefined && domain === "flytxt.com") {
                    var email = payload['email'];
                    var objectJson = {
                        'email': email
                    };         

                    var token = crypto.encrypt(JSON.stringify(objectJson));                   
                    res.status(200).send(JSON.stringify({ "status": "ok" , "token":token}));

                } else {
                    res.status(400).send(JSON.stringify({ "status": "error" , "message":"Invalid Login"}));
                }
            } else {
                res.status(400).send(JSON.stringify({ "status": "error" , "message":"Invalid Login"}));
            }
        });

};
