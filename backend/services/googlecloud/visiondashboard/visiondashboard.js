exports.visiondashboard = function dashboard(req, res) {
    var fs = require('fs');
    // var crypto = require('./crypto');
    // var jsonEncrypted = req.query.token;
    
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    res.header("Access-Control-Allow-Methods"," POST, PUT, DELETE, GET, OPTIONS");
    try {

        if (req.method == 'OPTIONS') {
            res.status(200).send(JSON.stringify({ "status": "ok" ,"message":"CROSS_REQUEST"}));
        }else {

            // var id = -1;
            // if ('id' in req.query) {
            //     id = req.query.id;
            // }
            // var json = crypto.decrypt(jsonEncrypted);
            // var jsonObject = JSON.parse(json);
            // var email = jsonObject.email;
            // console.log("Email",email,json);
            
            res.setHeader("Content-Type", "text/html");
            fs.readFile("index.html", function (error, pgResp) {
                if (error) {                    
                    res.status(200).send('Contents you are looking are Not Found');
                } else {
                    
                    res.status(200).send(pgResp);
                }
                
             
            });

        }

        


    } catch (err) {
        res.redirect('/visionlogin?signout=1');
    }

};

