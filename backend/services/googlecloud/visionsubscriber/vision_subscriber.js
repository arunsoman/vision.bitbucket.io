/**
 * Triggered from a message on a Cloud Pub/Sub topic.
 *
 * @param {!Object} event The Cloud Functions event.
 * @param {!Function} The callback function.
 */

const excelutils = require('./excel_utils.js');
const aidatastore = require('./datastore.js');
const fileUtils = require('./file_utils.js');

exports.visionsubscriber = (event, callback) => {
  console.log("subscriber event triggered");
  // The Cloud Pub/Sub Message object.
  const message = event.data;
  data = JSON.parse(Buffer.from(message.data, 'base64').toString());
  if(data.key=='manual_entry'){
  	fileUtils.process_file(data.fileBuffer,data);
  }else{
  	 excelutils.extractQuestions(data.filepath,data.sheetconfig,data.id,aidatastore,callback);
  }
  
};

