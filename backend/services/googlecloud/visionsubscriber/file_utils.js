const XlsxPopulate = require('xlsx-populate');
const datastore = require('./datastore.js');


exports.process_file = function (filebuffer,config) {
    var ques_arr = [];
    XlsxPopulate.fromDataAsync(Buffer.from(filebuffer, 'base64'))
        .then(workbook => {
            const value = workbook.sheet(0);
            header_row=value._rows[config.headerIndex];            
            var index_obj={
                quest:'',
                answ:'',
                feat:'',
                doc:''
            }
            for (let j = 1; j < header_row._cells.length; j++) {
                if(header_row._cells[j]._value==config.questionColumn){
                    index_obj.quest=header_row._cells[j]._columnNumber
                }
                else if(header_row._cells[j]._value==config.answerColumn){
                    index_obj.answ=header_row._cells[j]._columnNumber
                }
                else if(header_row._cells[j]._value===config.featureColumn){
                    index_obj.feat=header_row._cells[j]._columnNumber
                }
                else if(header_row._cells[j]._value===config.docRefColumn){
                    index_obj.doc=header_row._cells[j]._columnNumber
                }     
            }
                 
        
            var index=+config.headerIndex+1;           
	        for ( index; index < value._rows.length; index++) {
                var ques_obj = {
                    question: '',
                    answer: '',
                    docLink: '',
                    featureStatus: '',
                    productVersion:config.productVersion,
                    answeredTime : new Date(),
					answeredUser: config.email,
					requestStatus:'MANUAL_ENTRY',
					requestedSource:'MANUAL_ENTRY',
					uploadedFromFile:true,
					requestTime :new Date(),
                }

                if(value._rows[index]){
                    value._rows[index]._cells.forEach(function(inner){
                        if (inner._columnNumber == index_obj.quest) {
                                ques_obj.question = inner._value;
                            } else if (inner._columnNumber == index_obj.answ) {
                                ques_obj.answer = inner._value
                            } else if (inner._columnNumber == index_obj.feat) {
                                ques_obj.featureStatus = inner._value
                            } else if (inner._columnNumber == index_obj.doc) {
                                ques_obj.docLink = inner._value
                            }
                    })

                    if(!isBlank(ques_obj.question)){
                    	ques_arr.push(ques_obj);
                    }
                  	
                }
            }
           datastore.savequestions(ques_arr);
           
        });
}

function isBlank(str){
	return (!str || 0 === str.length);
}
