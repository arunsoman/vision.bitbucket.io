const Datastore = require('@google-cloud/datastore');
const projectId = 'complete-land-188108';

const datastore = new Datastore({
		projectId: projectId,
});

exports.datastore=datastore;


exports.savequestions = function(arr_entities){
	arr_entities.forEach(task => {	
		const taskKey = datastore.key('SOLUTION_REQUEST');
		const entity ={
			key: taskKey,
			excludeFromIndexes: ['question','answer'],
			data: task,
		};
		datastore.insert(entity).then(()=>{
			
		}).catch(err => {
			console.log("error",err);
    		
    	});

	});	
	
};


exports.saveRecord = function (arr_entities,id){
	// console.log("inside save record",arr_entities.length)
	arr_entities.forEach(value => {		
		const taskKey = datastore.key('AI_REQUESTS');
		const entity ={
			key: taskKey,
			excludeFromIndexes: ['question'],
			data:value,
		};
		
		datastore.insert(entity).then(()=>{
			
		}).catch(err => {
			console.log("error",err);
    		
    	});

	});	
	updateStatusToReady(id);

};
function updateStatusToReady(id){
	const taskKey = datastore.key(['RFP_REQUESTS', parseInt(id)]);
	datastore.get(taskKey).then(results =>{
          const task = results[0];
          // console.log(task);
          if (task){
            task.status = 'READY';
            const taskKey = datastore.key(['RFP_REQUESTS', parseInt(id)]);
			const entity ={
				key: taskKey,
				excludeFromIndexes: ['inputFile','sheets'],
				data: task,
			};
			return datastore.update(entity).then(()=>{
				console.log("RFP_REQUESTS is ready for business!..",id);
			});

        }
     });

}

exports.getRecord = function (taskId) {
	const taskKey = datastore.key(['AI_REQUESTS', parseInt(taskId)]);
	return datastore.get(taskKey); 
};

exports.getRFPRequest = function (taskId) {
	const taskKey = datastore.key(['RFP_REQUESTS', parseInt(taskId)]);
	return datastore.get(taskKey); 
};

exports.getQueryResult = function (query) {
	return datastore.runQuery(query);
};

exports.is_rfp_exist =function (id) {
	const query = datastore.createQuery('AI_REQUESTS').filter('templateRefId','=',id).limit(1);
	return datastore.runQuery(query);    			
};




