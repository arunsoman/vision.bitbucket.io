
exports.requestsolutionHttp = (req, res) => {
    exports.bplate(req, res, ()=>{});
  res.send(`requestsolutionHttp ${req.body.name || 'World'}!`);
};
exports.providesolutionHttp = (req, res) => {
    exports.bplate(req, res, ()=>{});
  res.send(`providesolutionHttp ${req.body.name || 'World'}!`);
};
exports.listallrequestsolutionHttp = (req, res) => {
    exports.bplate(req, res, ()=>{});
  res.send(`listallrequestsolutionHttp ${req.body.name || 'World'}!`);
};

exports.bplate=(req, res, handler)=>{
    if (req.body.message === undefined) {
    // This is an error case, as "message" is required
    res.status(400).send('No message defined!');
  } else {
    // Everything is ok
        handler();
    console.log(req.body.message);
    res.status(200).end();
  }
};