from flask import Flask, redirect, render_template, request ,url_for
from flask_cors import CORS
from rfp_request_handler import RFP_Request_Handler
import json
from serviceimpl import Serviceimpl
from concurrent.futures import ThreadPoolExecutor
import logging
import config
import os


#sudo pip install GoogleAppEngineCloudStorageClient -t /lib


# import flask_excel as excel
from flask import send_from_directory
from flytxt_chat_service import FlytxtChatService

app = Flask(__name__)
app.config.from_object(config)
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app)
executor = ThreadPoolExecutor(max_workers=5)
service = Serviceimpl()
flyChatService = FlytxtChatService()
# excel.init_excel(app)
ALLOWED_EXTENSIONS = set(['xlsx'])

def allowed_file(filename):
    return filename[-4:].lower() in ALLOWED_EXTENSIONS

@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


@app.route('/flyservices/upload', methods=['GET', 'POST','OPTIONS'])
def upload_file():

    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            print( '**found file', file.filename)
            file.save(os.path.join("data","live_chat", "input.xlsx"))
            return json.dumps({"message":"File successfully uploaded","status":"ok"});
    return json.dumps({"message":"File upload failed","status":"error"});


@app.route('/flyservices/reload',methods=['GET','OPTIONS'])
def flyservice_re_training():
    logging.info("re_train initiated")
    executor.submit(flyChatService.re_training_data_entities())
    results = {'status':'ok','message':"Retrain initiated"}
    return json.dumps(results)


@app.route('/flyservices/chatservice',methods=['POST','OPTIONS'])
def flyservices_chatservice():
    results = {}
    data = request.json
    if data:
        try:
            answers = flyChatService.get_answer(data['question'])
            results['answers'] = answers
            # results['product_versions'] = data['product_version']
            results['question'] = data['question']
        except Exception as err:
            logging.exception(err)
            results['answer'] = str("I am Not sure about it !!..")
            # results['product_versions'] = data['product_version']
            results['question'] = data['question']

    return json.dumps(results)

@app.route('/flyservices/download',methods=['GET','OPTIONS'])
def download_flyservices_excel():
    return send_from_directory(os.path.join('data','live_chat'), 'input.xlsx', as_attachment=True)

@app.route('/visionEndPoints/botservice',methods=['POST','OPTIONS'])
def assistant_service():
    results = {}
    data = request.json
    if data:
        try:
            answers = service.get_answer(data['question'])
            results['answers'] = answers
            # results['product_versions'] = data['product_version']
            results['question'] = data['question']
        except Exception as err:
            logging.exception(err)
            results['answer'] = str("I am Not sure about it !!..")
            # results['product_versions'] = data['product_version']
            results['question'] = data['question']

    return json.dumps(results)


@app.route('/visionEndPoints/versions',methods=['GET','OPTIONS'])
def version_service():
    results = {}
    try:
        results['product_versions'] = service.get_product_version()
    except Exception as err:
        logging.exception(err)
    return json.dumps(results)


@app.route('/visionEndPoints/versions',methods=['POST'])
def create_product():
    print("inside save service")
    results = {}
    data = request.json
    try:
        product = data['product']
        token = data['token']
        service.create_product(product)
        results['status'] = "ok"
        results['message'] = "Successfully process the record"
    except Exception as err:
        logging.exception(err)
        results['status'] = "Error"
        results['message'] = "Error while processing request"
    return json.dumps(results)

@app.route('/visionEndPoints/reload',methods=['GET','OPTIONS'])
def re_training():
    logging.info("re_train initiated")
    executor.submit(service.re_training_data_entities())
    results = {'status':'ok','message':"Retrain initiated"}
    return json.dumps(results)

@app.route('/visionEndPoints/download',methods=['GET','OPTIONS'])
def download_training_excel():
    return send_from_directory('data/train/neon/', 'input.xlsx', as_attachment=True)

@app.route('/visionEndPoints/ai',methods=['POST','OPTIONS'])
def vision_ai_end_points():
    results = {}
    remarks ={}
    data = request.json
    if data :
        try :
            template_ref_id = data['template_ref_id']
            token = data['token']
            executor.submit(RFP_Request_Handler(template_ref_id,token,service).execute)
            results['message']="RFP process initiated"
        except Exception as err:
            logging.exception(err)
            results['message'] = "Error while starting RFP process"
    return json.dumps(results)

if __name__ == '__main__':
    port = 8080
    app.run(host='192.168.127.81', port=port, debug=True)

