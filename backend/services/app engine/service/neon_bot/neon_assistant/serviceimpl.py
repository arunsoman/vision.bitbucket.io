
from document_service import Service
import logging


class Serviceimpl:

    def __init__(self):
        self.service = Service()

    def get_product_version(self):
        return self.service.get_product_version()

    def get_answer(self,text,product_version='neon'):

        answers = self.service.predict(text,product_version)
        answerArr = []
        if answers is not None and not answers.empty :
            for item in answers.iterrows():
                answerArr.append({'author': 'bot','type': 'text','answer': item[1]['Remark'], 'doc_ref': item[1]['Doc Ref'],
                                  'featureStatus': item[1]['Feature Status']})
        else:
            answerArr.append({'author': 'bot','type': 'text','answer': 'I am not sure about it!. Please contact my Tutor',
                              'doc_ref': '', 'featureStatus': 'I am not sure about it!'
                            })
        return answerArr
        # for item in  data.iterrows() :
        #     print (item)
        # return data.to_dict()

    def create_product(self,entity):
        from datastore import product_datastore
        product_datastore.create(entity)

    def re_training_data_entities(self,entities=None):
        from datastore import solution_response_store
        entities =  entities if entities else solution_response_store.get();
        self.service.re_load_data(entities);

    def re_training_data_entities_test(self,entities=None):
        self.service.re_load_data(entities);




#print (json.dumps(Serviceimpl().get_answer('CMS shall provide the ability to list all users who currently use or access the system.','p1')))


