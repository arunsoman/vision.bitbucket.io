import pandas as pd

class DataSet :

    def __init__(self,file_name):
        self.file_name = file_name
        self.dataFrame = self._parse_excel_(file_name)

    def _parse_excel_(self,file_name):
        dataFrame = []
        xl = pd.ExcelFile(file_name)
        for sheetName in xl.sheet_names:
            dataFrame = xl.parse(sheetName,keep_default_na=False)
        return dataFrame

    def get_document_array(self):
        return self.dataFrame['Question'].tolist()

    def get_doc_information(self,indices):
        return self.dataFrame.iloc[indices]

    def add_manual_data(self,dataframe):
        self.dataFrame = self.dataFrame.append(dataframe,ignore_index=True)

    def create_dataframe_from_entity(self,entities):
        df = pd.DataFrame(columns=['Question','Feature Status','Remark','Doc Ref','RFP Name','Product Version'],dtype='str')
        for entity in entities:
            df = df.append({'Question': str(entity.get('question')),'Feature Status': str(entity.get('featureStatus')),
                            'Remark': str(entity.get('answer')),'Doc Ref': str(entity.get('docLink')),
                            'Product Version': str(entity.get('productVersion')),
                            'RFP Name':str(entity.get('emplateRefId'))}, ignore_index=True)
        return df

    def save(self):
        self.dataFrame.to_csv('out.csv')




