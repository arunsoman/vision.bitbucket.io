from model import model
from nlp_dataset import DataSet
import os
import logging

class FlytxtChatService:

    def __init__(self):
        self.chat_model, self.data_set = self.load_models('data/live_chat')
        self.dir_path = 'data/live_chat'

    def predict(self,text,product_version='neon'):
        indices = None
        try:
            indices = self.chat_model.predict([text])
            return self.data_set.get_doc_information(indices) if indices else None
        except:
            print("Error while predict",indices,  text['richText'])

    def load_models(self, dir_path):
        dataset = DataSet(os.path.join(dir_path, 'input.xlsx'))
        chat_model = model()
        chat_model.train(dataset.get_document_array())
        return chat_model,dataset

    def get_answer(self, text, product_version='neon'):
        answers = self.predict(text, product_version)
        answerArr = []
        if answers is not None and not answers.empty:
            for item in answers.iterrows():
                answerArr.append(
                    {'author': 'bot', 'type': 'text', 'answer': item[1]['Answer'], 'category': item[1]['Type'],
                     'category_value': item[1]['Type_value']})
        else:
            answerArr.append(
                {'author': 'bot', 'type': 'text',
                 'answer': "Thanks for contacting us. We'll get back to you ASAP. Please share your email <<email box>>",
                 'category': 'form', 'category_value': 'email'
                 }
            )
        return answerArr

    def re_training_data_entities(self):
        print("re-train initiated")
        self.chat_model, self.data_set = self.load_models('data/live_chat')
        print("re-train completed")

