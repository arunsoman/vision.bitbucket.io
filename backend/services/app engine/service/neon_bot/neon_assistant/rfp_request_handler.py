from cloud_service_request import CloudServiceRequest
from serviceimpl import Serviceimpl

import time
import json
import threading

class RFP_Request_Handler():

    def __init__(self,rfp_request_id,token,ai_service):
        # threading.Thread.__init__(self)
        self.rfp_request_id = rfp_request_id
        self.token = token
        self.re_try_count=4
        self.ai_service = ai_service
        self.service_request = CloudServiceRequest()

    def execute(self):
        try:
            print ("Process Started")
            self.handleRFPRequests(self.rfp_request_id, self.token)
            print ("Process Completed")
        except Exception as e:
            print (e)

    def handleRFPRequests(self,rfp_request_id,token):
        responsejson = self.getRFPRequest(rfp_request_id,token)
        product_version = responsejson['product_version']
        sheets = json.loads(responsejson['sheets'])
        for sheet in sheets :
            self.processQuestions(self.getQuestions(rfp_request_id,sheet['sheet_name'],token),
                                  product_version) if sheet['skippable'] != 'true' else None

        self.update_record_status(rfp_request_id,token)

    def update_record_status(self, rfp_request_id, token):
        data = {'status': 'PROCESSED', 'id': rfp_request_id,'operationKey':'updateStatus'}
        response = self.service_request.put('uploaderendpoints', self.token, json=data)


    def getRFPRequest(self, rfp_request_id, token):

        params = {"id":rfp_request_id}
        for i in range(self.re_try_count):
            responsejson = self.service_request.get('uploaderendpoints', params, token)
            if responsejson:
                if responsejson['status'] == 'UPLOADED':
                    time.sleep(60)
                elif responsejson['status'] == 'READY':
                    return responsejson
                else:
                   raise ValueError("Unable to complete the process. Please check the status of RFP id  "+rfp_request_id,responsejson['status'])
            else :
                raise ValueError("No Record Found")


    def getQuestions(self,rfp_request_id,sheetName, token):
        data = {"templateRefId": rfp_request_id,'sheetName':sheetName}
        return self.service_request.post('visionstatustrackerendpoints', data, token)

    def updateQuestions(self,questions):
        incrementer = 50
        for i in range(0,len(questions),incrementer):
            end = len(questions) if i+incrementer > len(questions) else i+incrementer
            data = {'questions':questions[i:end],'postKey':'saveQuestions'}
            response = self.service_request.put('visionstatustrackerendpoints', self.token, json=data)

    def processQuestions(self,queations,product_version="neon"):
        for question in queations :
            answers  = self.ai_service.get_answer(question['question'])
            question['answers'] = json.dumps(answers)
            question['soultionProviedBy'] ='AI_ENGINE'
            question['finalStatus'] = 'AI_ENGINE'

        response = self.updateQuestions(queations)



#RFP_Request_Handler('5665117697998848','1fccd19d207874326a0bf705fbe909d5b9408cbaf0e1d17d60b141389d86742183',Serviceimpl())