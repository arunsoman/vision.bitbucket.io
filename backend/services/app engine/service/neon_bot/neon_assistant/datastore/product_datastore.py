from flask import current_app
from google.cloud import datastore


builtin_list = list


def init_app(app):
    pass


def get_client():
    return datastore.Client(current_app.config['PROJECT_ID'])


def from_datastore(entity):
    """Translates Datastore results into the format expected by the
    application.
    Datastore typically returns:
        [Entity{key: (kind, id), prop: val, ...}]
    This returns:
        {id: id, prop: val, ...}
    """
    if not entity:
        return None
    if isinstance(entity, builtin_list):
        entity = entity.pop()

    entity['id'] = entity.key.id
    return entity


def is_exist(product_name,product_version):
    ds = get_client()
    query = ds.query(kind='PRODUCT_VERSIONS')
    query.add_filter('product_name', '=', product_name)
    query.add_filter('product_version', '=', product_version)
    query_iter = query.fetch()
    for entity in query_iter:
        return True
    return False


def list(limit=10, cursor=None):
    ds = get_client()

    query = ds.query(kind='PRODUCT_VERSIONS', order=['product_name','version'])
    query_iterator = query.fetch(limit=limit, start_cursor=cursor)
    page = next(query_iterator.pages)

    entities = builtin_list(map(from_datastore, page))
    next_cursor = (
        query_iterator.next_page_token.decode('utf-8')
        if query_iterator.next_page_token else None)

    return entities, next_cursor


def read(id):
    ds = get_client()
    key = ds.key('PRODUCT_VERSIONS', int(id))
    results = ds.get(key)
    return from_datastore(results)


def update(data, id=None):
    ds = get_client()
    if id:
        key = ds.key('PRODUCT_VERSIONS', int(id))
    else:
        key = ds.key('PRODUCT_VERSIONS')

    entity = datastore.Entity(
        key=key
        #exclude_from_indexes=['description']
    )

    entity.update(data)
    ds.put(entity)
    return from_datastore(entity)


create = update


def delete(id):
    ds = get_client()
    key = ds.key('PRODUCT_VERSIONS', int(id))
    ds.delete(key)
