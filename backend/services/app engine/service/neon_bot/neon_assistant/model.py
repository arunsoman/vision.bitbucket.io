import nltk, string
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import numpy


class model:

    def __init__(self):
        nltk.download('punkt')
        self.stemmer = nltk.stem.porter.PorterStemmer()
        self.remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)
        self.vectorizer = TfidfVectorizer(tokenizer=self.normalize, stop_words='english')
        self.doc_vector = None


    def train(self,documents):
        self.doc_vector = self.__fit__(documents)


    def stem_tokens(self,tokens):
        return [self.stemmer.stem(item) for item in tokens]

    def normalize(self,text):
        return self.stem_tokens(nltk.word_tokenize(text.lower().translate(self.remove_punctuation_map)))

    def __fit__(self,documents):
        return self.vectorizer.fit_transform(documents)

    def predict(self,text_array,accuracy=.7):
        test_doc = self.vectorizer.transform(text_array)
        return numpy.where(numpy.asarray(cosine_similarity(test_doc,self.doc_vector)[0]) >accuracy)



