from bottle import Bottle, template, request,response
from rfp_request_handler import RFP_Request_Handler
import json
import sys
from serviceimpl import Serviceimpl
from concurrent.futures import ThreadPoolExecutor


def cors(func):

    def wrapper(*args, **kwargs):
        response.set_header("Access-Control-Allow-Origin", "*")
        response.set_header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
        response.set_header("Access-Control-Allow-Headers", "Origin, Content-Type")
        # skip the function if it is not needed
        if request.method == 'OPTIONS':
            return
        return func(*args, **kwargs)

    return wrapper


class Server:
    def __init__(self, host, port):

        self.service = Serviceimpl()
        self._host = host
        self._port = port
        self._app = Bottle()
        self.enable_cors()
        self._route()

    def _route(self):
        self._app.route('/visionEndPoints/botservice', method="POST", callback=self._botservice)
        self._app.route('/visionEndPoints/botservice', method="OPTIONS", callback=self._botservice)




    def start(self):
        self._app.run(host=self._host, port=self._port)


    def enable_cors(self):

        @self._app.hook('after_request')
        def enable_cors():
            """
            You need to add some headers to each request.
            Don't use the wildcard '*' for Access-Control-Allow-Origin in production.
            """
            response.headers['Access-Control-Allow-Origin'] = '*'
            response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
            response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'


    @cors
    def _botservice(self):
        results = {}
        postdata = request.body.read()
        if postdata :
            data = json.loads(postdata)
            try:
                answers = self.service.get_answer(data['question'])
                results['answers'] = answers
                # results['product_versions'] = data['product_version']
                results['question'] = data['question']
            except Exception as err:
                print (err)
                results['answer'] = str("I am Not sure about it !!..")
                # results['product_versions'] = data['product_version']
                results['question'] = data['question']
        return json.dumps(results)


args = sys.argv
system = '192.168.127.76'
port = 8081
if len(args) > 1:
    system = args[1]
if len(args) > 2:
    port = args[2]

server = Server(host=system, port=port)
server.start()