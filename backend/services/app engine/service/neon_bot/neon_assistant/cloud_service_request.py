import requests


class CloudServiceRequest:

    def __init__(self):
        self.base_url='https://us-central1-complete-land-188108.cloudfunctions.net/'

    def post(self,endpoints ,data,token):
        params= {"token":token}
        response = requests.post(url= self.base_url+endpoints,params=params,data=data)
        return response.json()

    def put(self,endpoints, token, data=None,json=None):
        params= {"token":token}
        response = requests.put(url= self.base_url+endpoints,params=params,data=data,json=json)
        return response.json()

    def get(self,endpoints,params,token):
        if not params :
            params = {}
        params["token"] = token
        response = requests.get(url=self.base_url + endpoints, params=params)
        return response.json()

# CloudServiceRequest().get('uploaderendpoints',None,'1fccd19d207874326a0bf705fbe909d5b9408cbaf0e1d17d60b141389d86742183')


