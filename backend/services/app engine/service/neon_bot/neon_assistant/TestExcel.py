
from __future__ import print_function
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools

from flask import Flask, redirect, render_template, request
from flask_cors import CORS
# import flask_excel as excel
from flask import send_from_directory
import openpyxl
import requests
from serviceimpl import Serviceimpl
import json,ast

service = Serviceimpl()

app = Flask(__name__)
#app.config.from_object(config)
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app)

@app.route('/visionEndPoints/reload',methods=['GET','OPTIONS'])
def add_training_excel():
    req = requests.get('https://us-central1-complete-land-188108.cloudfunctions.net/'
                       'visionendpoints?token=1fccd19d207874326a0be51efaea17d5a54a88bdc9e6d36b7bbb757092906d7b8a1693cc9eca8b')
    req.encoding = 'utf-8'

    json_data = ast.literal_eval(req.content)
    # json_data = json.loads(req.content)
    service.re_training_data_entities_test(json_data)



    # return Response(output.getvalue(), mimetype="text/csv")


@app.route('/visionEndPoints/download',methods=['GET','OPTIONS'])
def download_training_excel():
    req = requests.get('https://us-central1-complete-land-188108.cloudfunctions.net/uploaderendpoints?token=1fccd19d207874326a0bf70cfce4199ea70197a1c2e2ce6c54af596f8a916d2d9d579d818e&id=5757008003203072')
    data = req.json()
    data['inputFile']

    import pickle
    mybytes = data['inputFile']['data']
    with open("test.xlsx", "wb") as mypicklefile:
        pickle.dump(mybytes, mypicklefile)


    # wb = openpyxl.load_workbook(filename='data/train/neon/input.xlsx')
    # print (wb.worksheets)
    # ws = wb.worksheets[0]
    # i = ws.rows
    # print (i);
    # ws.append(["Test"]);
    # wb.save('data/train/neon/input.xlsx')
    return send_from_directory('data/train/neon/','input.xlsx',as_attachment=True)


if __name__ == '__main__':
    port = 8081
    app.run(host='127.0.0.1', port=port, debug=True)

