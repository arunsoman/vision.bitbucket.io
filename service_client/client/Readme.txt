This client application is used to upload the training data for Flytxt live chat, 
Which will replace the existing tarining data and retrain the models
 
How to Upload the updated training data ?
	1. Copy the updated training data excel file to "data\trainingdata.xlsx" file
	2. Open the client directory in terminal execute the command "python main.py"

Output 
	1. Download the current training file to the folder "backup\input_<time_date>.xlsx"
	2. upload the training data excel file from "data\trainingdata.xlsx"
	3. Once successfully upload the training file Re_train initiated 



Note : Please keep the current structure of training excel file.
