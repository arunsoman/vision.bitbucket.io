import requests
import datetime
#http://docs.python-requests.org/en/latest/user/quickstart/#post-a-multipart-encoded-file

#url = "http://192.168.127.81:8080/flyservices/"

url = 'https://complete-land-188108.appspot.com/flyservices/'


def download():
    print("* Existing Training data download request initiated")
    resp = requests.get(url + "download",stream=True)
    filename = 'backup/input_' + datetime.datetime.now().strftime("%I_%M%p_%B_%d_%Y") + '.xlsx'
    with open(filename, 'wb') as f:
        for chunk in resp.iter_content(2000):
            f.write(chunk)
    print("* Existing Training data downloaded to the file \n  "+filename)

def upload_training_data():
    print("* Start uploading the trainig data from  data/trainingdata.xlsx")
    fin = open('data/trainingdata.xlsx', 'rb')
    files = {'file': fin}
    try:
        r = requests.post(url + "upload", files=files)
        print ("* "+r.text )
        print("* Training data Successfully uploaded")

    finally:
        fin.close()

def retrain():
    try:
        print("* Invoke retrain ")
        resp = requests.get(url + "reload")
        print ("* "+resp.text)

    except:
        print("* Exception while retrain")


if __name__ == '__main__':
    print("*********************************************************************")
    download()
    upload_training_data()
    retrain();
    print("*********************************************************************")





